import { UI } from './UI/dom_builder.js';
import { dragElement } from './UI/drag_element.js';

export class Interface {
  #runes ={
    '<exit/>':'()=>{throw new Error("Code terminated from exit block")}',
    '<args>':'(',
    '<type>': '((type)=>jsml.checkType(',
    '</type>': ',type,"Input "))',
    '</args>':')=>',
    '<block>':'{',
    '</block>': '}',
    '<params>':'(',
    '</params>':')',
    '<curry>':'jsml.curry(',
    '</curry>':')',
    '<jsml>':' ',
    '</jsml>':' ',
    '<if>' : 'jsml.ifPredicate([', 
    '</if>': '])',
    '<...args/>':'(...args)=>',
    '<closure>':'((...args)=>{',
    '</closure>':'})',
    '<comp>':'jsml.comp(',
    '</comp>':')',
    '<pipe>':'jsml.pipe(',
    '</pipe>':')',
    '<js>':' ',
    '</js>':' ',
    '<iife>':'(()=>{',
    '</iife>': '})()',
    '->':'return',
    '<./>':',',
    '<i/>':'()',
    '<void>':'((input)=>{',
    '</void>': '})',
    '<fold>': 'jsml.fold((output,current,index,input)=>{',
    '</fold number>':';return output},new Number()})',
    '</fold string>':';return output},new String())',
    '</fold array>':';return output},new Array())',
    '</fold boolean>':';return output},new Boolean())',
    '</fold map>':';return output},new Map())',
    '</fold set>':';return output},new Set())',
    '</fold object>':';return output},new Object())',
    '<tco>': 'jsml.tco(()=>{',
    '</tco>' :'});return ',
    '<scan>':'jsml.scan((current,index,input)=>{',
    '</scan>': '})',
    '<some>':'jsml.some((current,index,input)=>{',
    '</some>' : '})',
    '<find>':'jsml.find((current,index,input)=>{',
    '</find>' : '})',
    '<every>':'jsml.every((current,index,input)=>{',
    '</every>' : '})',
    '<sort>':'jsml.sort((a,b)=>{',
    '</sort>': '})',  
    '<filter>':'jsml.filter((current,index,input)=>{',
    '</filter>': '})',
    '<boolean>': 'jsml.io((input)=>{',
    '</boolean>': '},new Boolean())',
    '<func>': 'jsml.io((input)=>{',
    '</func>': '},new Function())',
    '<HTMLElement>': '((el)=>jsml.io((input)=>{',
    '</HTMLElement>': '},document.createElement(el)))',
    '<number>': 'jsml.io((input)=>{',
    '</number>': '},new Number())',
    '<string>': 'jsml.io((input)=>{',
    '</string>': '},new String())',
    '<array>': 'jsml.io((input)=>{',
    '</array>': '},new Array())',
    '<object>': 'jsml.io((input)=>{',
    '</object>': '},new Object())',
    '<set>': 'jsml.io((input)=>{',
    '</set>': '},new Set())',
    '<map>': 'jsml.io((input)=>{', 
    '</map>': '},new Map())',
  };
  
  #nameSpace = 'jsml';
  #editor;
  #output;
  #documentation = new Map();
  #LineOffset = 4;
  changesStack = [];
  tabs = [];
  code={fullSource:'',JSandJSML : '',JSML_Tags : '', javascript:'', standatLib :`const ${this.nameSpace} = (() => { const pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x),comp = (...fns) => fns.map((f)=>f()),ifPredicate = (conditions) => conditions.map((cond)=>cond[0]() ? cond[1]() : false), convertToArray = (input)=>Array.isArray(input) ? input : input.constructor.name==="Object" ? input.length>=0 ?  Array.from(input).fill(null) : Object.entries(input) : !isNaN(input) ? (input+"").split("") : Array.from(input), checkType = (res,init,errorPrefix="Output ")=>{ if(res===undefined){return;}else if(res.constructor.name === init.constructor.name){ return res} else { throw new Error(errorPrefix + " expected type of " + init.constructor.name + " but instead got "+ res.constructor.name ) }}, curry = function (func) {function curried(...args) {if (args.length >= func.length) {return func.apply(this, args);} else {return function(...args2) {return curried.apply(this, args.concat(args2));}}};return curried.bind(this);}, io=(callback,init)=>(input)=>checkType(callback(input,init),init),filter = (callback,init) => (input) => convertToArray(input).filter(callback),every=  (callback) => (input) =>convertToArray(input).every(callback),some=(callback) => (input) =>convertToArray(input).some(callback),find=(callback) => (input) =>convertToArray(input).find(callback),sort = (callback) => (input) => convertToArray(input).sort(callback), scan = (callback) => (input) => convertToArray(input).map(callback), fold = (callback, init) => (input) => checkType( convertToArray(input).reduce(callback,init),init);class Tco {constructor(func) {this.func = func;}execute() {let value = this;while (value instanceof Tco)value = value.func();return value;}};const tco = (f) => new Tco(f); return {io,tco,filter,sort,every,find,some,scan, fold, ifPredicate,curry,pipe,checkType,comp}})() \n`};
  width;
  height;
  isAppOpened = false;

  constructor(divEl, outputEl, options) {
    this.#editor = CodeMirror(divEl, options);
    this.#output = outputEl;
  }
  get nameSpace(){
    return this.#nameSpace;
  }
  get runes(){
    return this.#runes;
  }

  get editor() {
    return this.#editor;
  }

  get cursor() {
    return { ...this.editor.getCursor() };
  }

  get documentation() {
    return this.#documentation;
  }
  setSize = (x,y) => {
    this.editor.setSize(x, y);
    this.width = this.editor.getWrapperElement().clientWidth
    this.height = this.editor.getWrapperElement().clientHeight
  }
  setRunes(runes){
this.#runes = runes
  }
  log(value, color) {
    this.#output.value = value;
    this.#output.style.color = color;
  }
  parsePipeCommas(source){
    const pipeContent = source.match(/<pipe>(.|\n)*?<\/pipe>/)[0]
    if(pipeConent){
      source = source.replace(pipeContent, pipeContent.split('.>\n').join('>,\n'))
    }

   return res;

  }
  parse(source){
   const runesRgx = Object.keys(this.runes).join('|');
   const RuneRgx = new RegExp(runesRgx,"g")
    return source.replace(RuneRgx, (matched)=>{
      if(this.runes[matched]){
        return this.runes[matched];
      }else{
        return matched 
      }
    
    })
  }
//   open:function(file='app'){
//      setTimeout(()=>{   
// const win = window.open(file+'.html',file.toUpperCase(),'toolbar=no,width=600,height=500,left=700,top=200,status=no,scrollbars=no,resize=no');
// return setTimeout(()=>{
//   win.document.write(this.raw()); this.resetSettings();
// },this.buidDelay);
//      },this.openDelay);  
// }

openWindow(data,file='app'){
  setTimeout(()=>{
 const iframe =  document.getElementById('app_window');
//  const url = document.getElementById('url-input').value
  const app_window =   iframe.contentWindow
//  if(url){
//    iframe.src = url;
//    iframe.src = iframe.src
//   // iframe.contentWindow.location.reload();
//  }else{
   app_window.location.reload();
   return setTimeout(()=>{
     app_window.document.write(data);
     //app.resetSettings();
     //win.document.write(app.raw()); app.resetSettings();
     },100);
//  }
   },0);  
   }
buildApp(){
  const html =  this.code.transpiledHTML;
  const htmlContent = html.split('<jsml/>')
  const htmlContentA = htmlContent[0]
  const htmlContentB = htmlContent[1]
  //const jsmlTagContent = htmlContent[1].split('</jsml>')[0];
  const htmlOutput = `${htmlContentA}\n<script>${this.code.standatLib}</script><script>
  const  ${this.nameSpace}Source = (()=>{\n${this.code.javascript.trim()}\n})\n ${this.nameSpace}Source(); \n</script>\n${htmlContentB}`
  this.openWindow(htmlOutput)
}
  
  runCode() {
    try {
           this.code.fullSource = this.editor.getValue();
           const codeSplit = this.code.fullSource.split('</html>')
           const isHtml = codeSplit.length>1;
           const html = codeSplit[0];
           const source = isHtml ? codeSplit[1] : codeSplit[0]
        
      const tabs = this.tabs.length>0 ? this.tabs.join('\n') : '';
      const full = tabs + '\n' + source;
      this.code.JSML_Tags = source;
      const replaced = this.parse(full);
      this.code.javascript = replaced;
      if(isHtml && this.isAppOpened){
        this.code.transpiledHTML = html +'</html>';
        this.buildApp()
      }else{
        const parsed = this.code.standatLib + this.code.javascript;
        this.code.JSandJSML = parsed;
        const selection = this.editor.getSelection().trim()
        const result = new Function(`"use strict"; return ((()=>{ ${parsed}; return ${selection ? this.parse(selection) : undefined }})())`)();
        this.log(JSON.stringify(result), 'rgb(85, 226, 100)');
      }

    } catch (err) {
      const stack = err.stack.split('<anonymous>:');
      if (err.stack && stack[1]) {
        const offset = this.#LineOffset;
        const stackLen = stack.length;
        const errCoords = err.stack
          .split('mous>:')
          [stackLen - 2].split(')')[0]
          .split(':');
        const line = errCoords[0] - offset;
        const ch = errCoords[1];
        const ERROR_MSG =
          err.stack.split('Error:')[0] +
          'Error at line ' +
          line +
          ': ' +
          err.message;
        const errorEl = UI.create('button', {
          id: 'error_line',
          innerHTML: ' ' + err.message,
        });
        this.editor.addWidget({ line, ch: Infinity }, errorEl, true);
        setTimeout(() => {
          errorEl.parentNode.removeChild(errorEl);
        }, 3000);

        this.editor.focus();
        this.log(ERROR_MSG, '#ff275d');
      } else {
        this.log(
          err.stack.split('Error:')[0] + 'Error: ' + err.message,
          '#ff275d'
        );
        console.error(err);
      }
    }
  }
  openAppWindow() {
   if(!this.isAppOpened){
 
    const appWin = document.getElementById('app_window');
    // this.editor.display.input.textarea.style.width = '50%';
 
    this.editor.setSize(this.width-750,this.height )
    appWin.style.display = 'block'
    // document.getElementById('url-input').style.display = 'inline-block'
    // document.getElementById('windowMode').style.filter = 'grayscale(0)';
   }else{
    const appWin = document.getElementById('app_window');
    // document.getElementById('url-input').style.display = 'none'
    this.editor.setSize(this.width , this.height )
    // this.editor.display.input.textarea.style.width = '90%';
   
    appWin.style.display = 'none';
    // document.getElementById('windowMode').style.filter = 'grayscale(100)';
   } 
   this.isAppOpened=!this.isAppOpened;
  }
  changeTheme(theme) {
    this.editor.setOption('theme', theme);
    location.hash = '#' + theme;
  }
  // getSnippetsFromExternal(external) {
  //   if (external) {
  //     const link = `https://raw.githubusercontent.com/AnthonyTonev/hyper_code_snippets/master/${external}.json`;
  //     fetch(link)
  //       .then((response) => response.json())
  //       .then((data) => {
  //         this.editor.setValue(
  //           this.editor.getValue() + '\n' + data.code + '\n'
  //         );
  //         this.editor.scrollIntoView({ line: this.editor.lastLine(), char: 0 });
  //         this.log(data.title + ' imported!', '#ffffff');
  //         if (data.description) {
  //           data.description.map((item) => {
  //             this.#documentation.set(item.id, {
  //               description: item.description,
  //               example: item.example,
  //             });
  //           });
  //         }
  //       })
  //       .catch((err) => {
  //         this.log(
  //           'Snippet does not exist or it has bad JSON format.',
  //           '#f3e110'
  //         );
  //       });
  //     return;
  //   }
  // }
  addLegend(){
    const legend = `/*

${Object.entries(this.#runes).reduce((acc,item)=>{
acc+=item[0] + '        ' + item[1] + '\n';
      return acc;
    },'')}

*/
    
    `
    this.openTabs(legend,67,40);
    // this.editor.setValue(legend+ '\n'+ this.editor.getValue())
  }
  openTabs(val,cols=45,rows=25) {
    const selection = val ? val : this.editor.getSelection();
    this.editor.replaceSelection('');
    this.tabs.push(selection);
 
    const div = UI.create('div', {
      class: 'tab_box',
      style: `background-color:rgba(22,22,22,0.15);`,
    });
    const dragg = UI.create('button', div, {
      class: 'ui',
      style:
        'cursor:pointer;font-size:30px;width:50px;height:50px;text-align:center;',
      textContent: '⁜',
      drag: 'false',
    });
    UI.create('p', div);
    UI.create('button', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;cursor:pointer',
    
      innerHTML:' << '
    }).addEventListener('click',(e)=>{
   
      this.editor.setValue(this.editor.getValue()+'\n'+this.tabs[e.target.value-1].trim());
      this.tabs[e.target.value-1] = "";
      document.getElementById('tab_'+e.target.value).value = '';
    });
    UI.create('input', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;',
     
    })
    UI.create('button', div, {
      class: 'ui',
      value: this.tabs.length,
      style: 'text-align:center;cursor:pointer;',
      innerHTML:' >> '
    }).addEventListener('click',(e)=>{
      const selection = this.editor.getSelection().trim();
      this.editor.replaceSelection('');
      this.tabs[e.target.value-1] = selection;
      document.getElementById('tab_'+e.target.value).value = selection;
    });
    dragg.addEventListener('click', () => {
      const drag = dragg.getAttribute('drag');
      if (drag === 'false') {
        dragg.setAttribute('drag', 'true');
        dragg.textContent = '❖';
      } else {
        dragg.setAttribute('drag', 'false');
        dragg.textContent = '⁜';
      }
    });
    UI.create('p', div);

    UI.create('textarea', div, {
      cols:cols,
      rows: rows,
      id:'tab_'+this.tabs.length,
      class: 'tab_line',
      innerHTML: this.tabs[this.tabs.length-1],
    });

    dragElement(div, dragg);
    this.editor.addWidget(this.cursor, div, true);
  }
}


