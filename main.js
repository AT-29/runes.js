import { UI } from './UI/dom_builder.js';

import { Interface } from './editor.js';

const clearNodesByClass= (className='log_box') => {
  console.clear();
  [...document.getElementsByClassName(className)].forEach((item) =>
    item.parentNode.removeChild(item)
  );
};


window.addEventListener('load', () => {
  UI.setParent(document.getElementById('app_container'));

  UI.create('button', {
    textContent: '▷',
    class: 'ui',
    style: 'width:55px;heigth:25px; margin-right:1%',
  }).addEventListener('click', () => Code.runCode());
  UI.create('button', {
    id:'windowMode',
    innerHTML: '&#9639;',
    class: 'ui',
    style: 'width:55px;heigth:25px; margin-right:1%',
  }).addEventListener('click', () => Code.openAppWindow());



  UI.create('input', {
    id: 'console',
    disabled: true,
    class: 'ui',
    style: 'width:80%',
  });

  UI.create('iframe',{id:"app_window"})

  const colorThemes = document.getElementById('colthemes');

  const Code = new Interface(
    document.getElementById('app_container'),
    document.getElementById('console'),
    {
      value: '',
      mode: 'htmlmixed',
      tabSize: 5,
      theme: 'night',
      lineNumbers: true,
      styleActiveLine: true,
      matchBrackets: true,
      onGutterClick: CodeMirror.newFoldFunction(CodeMirror.braceRangeFinder),
      extraKeys: {},
      gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
      lint: false,
      foldGutter: true,
    }
  );
  Code.setSize(1750, 800);

  const themeChoice =
    (location.hash && location.hash.slice(1)) ||
    (document.location.search &&
      decodeURIComponent(document.location.search.slice(1)));
  if (themeChoice) {
    colorThemes.value = themeChoice;
    Code.editor.setOption('theme', themeChoice);
  }
  CodeMirror.on(window, 'hashchange', function () {
    const theme = location.hash.slice(1);
    if (theme) {
      colorThemes.value = theme;
      Code.changeTheme(
        colorThemes.options[colorThemes.selectedIndex].textContent
      );
    }
  });

  

  colorThemes.addEventListener('change', () =>
    Code.changeTheme(colorThemes.options[colorThemes.selectedIndex].textContent)
  );

  

  document.addEventListener('keydown', (e) => {
    if (e.key.toLowerCase() === 'q' && e.ctrlKey) Code.runCode();
  });

  UI.create('div');

  UI.create('button', {
    textContent: 'js',
    class: 'ui',
  }).addEventListener('click', (e) =>{
    if(e.target.textContent==='js'){
      e.target.textContent = 'JSML';
      Code.editor.setValue(Code.code.JSandJSML);
    }else{
      e.target.textContent = 'js';
      Code.editor.setValue(Code.code.fullSource);
    }

  } );


  UI.create('button', {
    innerHTML: '{...}',
    class: 'ui',
  }).addEventListener('click', (e) =>{
    if(e.target.innerHTML==='{...}'){
      e.target.innerHTML = '{&crarr;}';
      CodeMirror.commands.foldAll(Code.editor);
    }else{
      CodeMirror.commands.unfoldAll(Code.editor);
      e.target.innerHTML = '{...}';
 
    }

  } );
 

  UI.create('button', {
    textContent: '++tablets',
    class: 'ui',
  }).addEventListener('click', () => Code.openTabs());
  UI.create('button', {
    textContent: '--tablets',
    class: 'ui',
  }).addEventListener('click', () =>{
    clearNodesByClass('tab_box');
    Code.tabs.length = 0;
  } );
  UI.create('button', {
    textContent: 'legend',
    class: 'ui',
  }).addEventListener('click', () =>{
    Code.addLegend();
  } );

  //thisi s just because i'm lazy to make it dynamic;
  document.getElementById('colthemes').style.display = 'inline-block';



  
});

