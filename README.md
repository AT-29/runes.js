```

    '<args>':'(',
    '</args>':')=>',
    '<block>':'{',
    '</block>': '}',
    '<params>':'(',
    '</params>':')',
    '<curry>':'_curry_(',
    '</curry>':')',
    '<jsml>':' ',
    '</jsml>':' ',
    '<if>' : '_ifPredicate_(',
    '</if>': ')',
    '<closure>':'((...args)=>{',
    '</closure>':'})',
    '<pipe>':'_pipe_(',
    '</pipe>':')',
    '<js>':' ',
    '</js>':' ',
    '<iife>':'(()=>{',
    '</iife>': '})()',
    '->':'return',
    '<./>':',',
    '<i/>':'()',
    '<void>':'((input)=>{',
    '</void>': '})',
    '<fold>': '_reduce_((output,current,index,input)=>{',
    '</fold number>':';return output},new Number())',
    '</fold string>':';return output},new String())',
    '</fold array>':';return output},new Array())',
    '</fold boolean>':';return output},new Boolean())',
    '</fold map>':';return output},new Map())',
    '</fold set>':';return output},new Set())',
    '</fold object>':';return output},new Object())',
    '<tco>': '_tco_(()=>{',
    '</tco>' :'});return ',
    '<scan>':'_map_((current,index,input)=>{',
    '</scan>': '},new Array())',
    '<every>':'_every_((current,index,input)=>{',
    '</every>' : '},new Boolean())',
    '<sort>':'_sort_((a,b)=>{',
    '</sort>': '},new Array())',  
    '<filter>':'_filter_((current,index,input)=>{',
    '</filter>': '},new Array())',
    '<boolean>': '_io_((input)=>{',
    '</boolean>': '},new Boolean())',
    '<func>': '_io_((input)=>{',
    '</func>': '},new Function())',
    '<number>': '_io_((input)=>{',
    '</number>': '},new Number())',
    '<string>': '_io_((input)=>{',
    '</string>': '},new String())',
    '<array>': '_io_((input)=>{',
    '</array>': '},new Array())',
    '<object>': '_io_((input)=>{',
    '</object>': '},new Object())',
    '<set>': '_io_((input)=>{',
    '</set>': '},new Set())',
    '<map>': '_io_((input)=>{',
    '</map>': '},new Map())',
  
  
```

```
-> ((n)=> <pipe><fold><js> 
index > 0 ? output[index-1] = index%15 ? index%5 ? index%3 ? index : 'Fizz' : 'Buzz' : 'FizzBuzz' : null
</js></fold array> )({length:n+1})</pipe>(15)
```


```
-> <pipe>
       <fold> 
        output[index] = Array.from({length:5}).fill(null)
       </fold array> <./>
        <fold> 
        	output[index] = <fold> 
                           output[index] = index 
          </fold array><params>input[index]</params> 
  	  </fold array>
</pipe><params>{length:15}</params>
```

```
-> <number> const fact = (n, acc) => <tco>
    <js>
    if (n < 2) -> acc
    else -> fact(n-1, n * acc) </js>
  </tco> fact(input, 1).execute() </number><params>5</params>
```
```
-> <number> const fact = (n, acc, memo) => <tco>
    <js>
    if (n < 2) -> acc
    else -> memo.has(n) ? memo.get(n) : memo.set(n,fact(n-1, n * acc,memo)) && memo.get(n)  
    </js>
  </tco> fact(input, 1, new Map()).execute() </number>
<params>5</params>
```

```
-> <pipe>
  <number> -> input*2 </number>  <./>
  <number> -> input*12 </number> <./>
  <number> -> input-2 </number>  
</pipe>(5)
```


```
-> <pipe>
<object>  
const list = input;
const reverse =  (list,prev=null) =>
<tco> 
if(!list.next){
list.next = prev; 
 -> list; 
}	 
const res = reverse(list.next,list)
list.next = prev;
-> res;
</tco> reverse(list).execute()
  </object> </pipe>({value:1,next:{value:2,next:{value:3,next:{value:4,next:null}}}})
```


```
<js> const filterGrades = (value) => (item)=> item.grade > value </js>

const groupTech = <args>predicat = () => true </args>
	  <pipe>
        <scan> -> current[1] </scan> <./> 
        <array> 
          	-> <scan> 
          		-> <fold>
          			<js>
                          if(predicat(current)){	
                          if(output.has(current.tech)) output.add(current.tech)
                          else output.add(current.tech) 
                          }</js>
          		   </fold set>(current)
                  </scan><params>input</params>
	  </array> <./>
       <array> const [group1,group2] = input 
       ->  <fold>
           if(group1.has(current)) output.push(current)  
        	 </fold array><params>[...group2.keys()]</params> 
       </array>
	</pipe>
  <params>{
  Team1:[
  {name:"Maria",tech:'JS', grade:5},
  {name:"Asen",tech:'C#', grade:2},
  {name:"Lubo",tech:'CJ', grade:6}, 
  ],
  Team2:[
  {name:"Ivan",tech:'C#', grade:2}, 
  {name:"James",tech:'C#', grade:4},  
  {name:"Niki",tech:'JS', grade:3},
  ]}</params>

-> groupTech<params> filterGrades(2) </params>
```

```
const add = (a) => (b)=> a+b
const func =  (init, offset) => <pipe> 
<number> -> add(offset)(input-2) </number> <./>
<number> -> add(offset)(input*2) </number> <./>
<number> -> add(offset)(input*22) </number> 
</pipe> <params> init </params>
-> func(5,13)
```

```
->  <pipe> <sort> -> b - a </sort> <./>
         <scan> -> current*10 </scan> <./>
         <fold> output+=current </fold number> <./>
         <fold> output[index] = +current </fold array> <./>
         <fold> output+=current  </fold number> 
</pipe>
([2,3,1,6]) 
```


```
-> <pipe>
<string><js>
    const s = input
    if(!input) -> 0
    if(!input.trim() || input.length===1) -> 1
    else -> input
</js></string> <./> 
  <number><js>
    const s = input
    let memo = new Set()
    const repeat = (index,rec,count) => </js><tco><js>
   	   if(!s[index]) -> Math.max(memo.size,rec)
        if(memo.has(s[index])){
          rec = Math.max(memo.size,rec)
          memo.delete(s[count])
          count += 1
          memo = new Set(memo)
        }else{
         memo.add(s[index]) 
         index+=1
        }
      -> repeat(index,rec,count)
</js></tco><js> repeat(0,0,0).execute() </js></number>
</pipe>('xczsd')
```

```
<js>
const calc = (input) =>{
const x = 3;
const y = 5;
const z = 3;
->  Math.sqrt((input*x)/y,z)
}
const add3 = (input) =>input+3
const mult10 = (input) =>input*10
const toFixed2 = (input) =>input.toFixed(2)
const convertToNum = (input) =>+input
const splitDot = (input) =>input.split(".")
</js>             
                            
const myPype = <pipe>
    <number> -> calc(input) </number> <./>
    <number> -> add3(input) </number> <./>
    <string> -> toFixed2(input) </string> <./> 
    <array>  -> splitDot(input) </array> <./>
    <fold> output += current </fold string>  <./>                              
    <number> -> convertToNum(input) </number>                             
</pipe>
-> myPype(3)
```

```
const removeElement = <closure>
  <js>
  const [nums,target] = args;
   const remove = (index) => </js><tco>
    <js>
        if(nums[index]===target){
           nums[index] = nums[nums.length-1]
 		 nums[nums.length-1] = nums[index]
           nums.length-=1;
        }
       if(index===0) -> nums.length;
       -> remove(index-1)
    </js>
</tco><js>remove(nums.length).execute() </js>
</closure>
-> removeElement([0,1,2,2,3,0,4,2],2)        
```

```
<js>
const nums = [1,3,4,1,6,2]
const addprev = (current,index,input) => current+=input[index-1] || 0
const addnext = (current,index,input) => current+=input[index+1] || 0
</js>
  -> <pipe>
    <scan> -> addprev(current,index,input)</scan> <./>
    <scan> -> addnext(current,index,input)</scan> <./>
    <fold> output+=current </fold number>  
  </pipe>(nums)

```

```
<js>
const array = [1,2,3,4,5,6]
const a = (current,index) => current+index*10
const b = (current,index) => index+1
const c = (current,index) => current+index+1
</js>
-> <pipe>
  		<scan> -> a(current,index)</scan> <./>
		<scan> -> b(current,index)</scan> <./>
  		<fold>output+=c(current,index)</fold number> <./>
  		<fold>output+=c(current,index)</fold string> <./>
  		<scan> -> +current</scan> <./>
  		<fold>output+=c(current,index)</fold number> <./>
  		<fold>output+=c(current,index)</fold string> <./>
  		<scan> -> +current</scan> <./>
  		<fold>output+=c(current,index)</fold number> <./>
  		<fold>output+=c(current,index)</fold string> <./>
  		<scan> -> +current</scan> <./>
  		<fold>output+=c(current,index)</fold number> 
	</pipe>
(array)
```

```
<js>
const array = [1,2,3,4,5,6]
const a = (current,index) => current+index*10
const b = (current,index) => index+1
const c = (current,index) => current+index+1
</js>
-> <pipe>
  		<scan>
               -> <pipe>
                <scan> -> b(current,index)</scan> <./>
                <fold>output+=c(current,index)</fold number> <./>
                <fold>output+=c(current,index)</fold string> <./>
            </pipe> <params> current </params>
          </scan> <./>
  	<fold>output+=Number(a(current,index))</fold number>
	</pipe>
(array)
```

```
-> <number>
     <if><js>[
          [()=>input>5 , ()=>alert('yes')],
          [()=>input>12 , ()=>alert('hhh')],
          [()=>input>5 , ()=>alert('sss')]
       ]</js></if>
  -> 0
</number><params> 7 </params>

```

```
const myReasonToBeleave = () => <pipe>
            <string>
             -> input + ' World'
            </string><./>
             <string>
             -> input + '!!!!'
            </string><./>
  		 <array>
  		-> input.split('!')
  		</array><./>
  		 <scan> -> current+'*' </scan>
	</pipe>
-> myReasonToBeleave()('hello')


```


```
-> <pipe>
  		<filter>
           -> !isNaN(current) 
  		</filter>
  		<./>
  		<every>
  		-> !isNaN(current)
  		</every>
  		<./>
  		<fold>
            output[index] = current 
  		</fold array>
  		<./>
  		<string>
  		-> input.join("")
  		</string>
  		<./>
  		<boolean>
  		-> !input
  		</boolean>	
		</pipe>
([1,2,3,4,"dsada"])
```

```
 <js>
  const stringToArray = (str) => str.split('')
  const convertToNum = (str) => Number(str) || 0
  </js>
-> <pipe>
  <array> -> stringToArray(input)</array> <./>
  <scan> -> convertToNum(current)</scan> <./>
  <fold>output+=current</fold number>
  </pipe>
('123413516')

```

```
const fn = <curry><args> a,b,c,d </args> 
			<block>
                 -> <number> -> (a+b+c+d) </number>
  			</block></curry>
-> fn(1)(1)(2)(3)()
```

```
const x = <curry>
  		<args>a,b,c,d</args>
  		<block>
            -> <number> 
            	-> ((a + b)/c)*d
            	</number>
  		</block>
          </curry>
-> x(1)(2)(3)(4)()
```

```
const fn = <curry><args> a,b,c,d </args> 
			<block>
                 -> <number> -> (a+b+c+d) </number>
  			</block></curry>


-> <pipe>
   <number> -> fn(1)(2)(3)(4)(input) </number> <./>
   <number> output = input+10</number> <./>
   <fold> output += Number(current) </fold number>	
</pipe><params>10</params>


```


```
const fn = <curry><args> a,b,c,d </args> 
		 <block> 
             ->  (a+b+c+d) 
  		 </block></curry>

-> <pipe>
   <func> -> fn(1) </func> <./>
   <func> -> input(2) </func> <./>
   <func> -> input(3) </func> <./>
   <number>  -> input(4) </number><./>
   <number> -> input+10</number> <./>
   <fold> output += Number(current) </fold number>	
</pipe><params>10</params>


```

```
const counter = <void>
  let count = 0
  -> <args></args>
  	<block>
       -> <number> -> count +=1 </number>()
 	</block>
  </void>
const count = counter();
count(); 
count(); 
count(); 
count();  
count();
-> count()
```
```
const counter = <void>
  let count = 0
  -> <void>
       -> count +=1
 	 </void>
  </void>
<js>
const count = counter();
</js>
-> <number> -> count()</number><i/>
```

```
const counter = <void>
  let count = 0
  -> <void>
       -> count +=1
 	 </void>
  </void>
<js>
const count = counter()
const inc = (input)=>input+1
const createArr = (input,offset) => [...new Array(input)].map((_,index)=>index+offset)
const numToArr = (input) => input.toString().split("") 
</js>
-> <pipe>
  	<number> -> inc(input)</number><./>
  	<number> -> inc(input)</number><./>
  	<number> -> inc(input)</number><./>
  	<number> -> inc(input)</number><./>
  	<array> -> createArr(input,1)</array><./>
     <pipe>
       <scan> -> inc(current)</scan><./>
       <fold> output+=inc(current)</fold number><./>
       <fold> output[index] = +current</fold array><./>
       	   <pipe>
       		<scan> -> inc(current)</scan><./>
       		<fold> output+=inc(current)</fold number>
     	  </pipe>
     </pipe><./>
    <array> -> numToArr(input) </array><./>
    <scan> -> +current</scan>
</pipe><params>0</params>
```


```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
    <style>
  .UI{
    	border:solid 2px black;
    	font-weight: 900;
     background:transparent;
    }
  </style>
</head>
<body>
  <p>Staticly added HTML</p>
  <h2>JSML Todo App</h2>
</body>
<jsml/>
</html>

<jsml>
  const createElement = <curry>
        				<args>type,parent,params</args>
  										<block>
                    <if>
                        [[<boolean> ->!type </boolean>,<exit/>],
                        [<boolean> ->!parent </boolean>,<exit/>]]
                    </if>
      const element = document.createElement(type);
      parent.appendChild(element)
      if (params)<scan>
      <js>
      const [key,value] = current
      if(key !== 'textContent' && key !== 'innerHTML') element.setAttribute(key, value)
      else element[key] = value
      </js></scan><params>params</params>
      -> element
  										</block>
  					</curry>
  const selectElementsById = <...args/>
  					 <block>
                            -> <scan>
                            -> document.getElementById(current)
                            </scan><params>args</params>
  					</block>
  
  const addRawHTML = <args>parent,content</args>
  				 <block>
  -> createElement('div',parent,{innerHTML:content})
  </block>
  
  const todoComponent = <args>methods,elements</args>
  				    <block>
                          <js>
    const {createTodo,destroyTodos,addTodoProps} = methods();
    const [addTodoEl,inputField,destroyTodosEl,todoContainer] = 
  selectElementsById(...elements)
                          </js>

  if(addTodoEl){
  const outputValues = <void>
 -> <pipe>
  <HTMLElement> 
  -> createElement('div',input,{class:'todo'})
  </HTMLElement><params>'div'</params><./>
  <func> -> createTodo(input)</func><./>
  <HTMLElement> -> addTodoProps(input,inputField.value)</HTMLElement>
  <params>'p'</params><./>
  <func> -> createElement('input',input)</func><./>
  <HTMLElement> 
   -> input({type:"checkbox",isChecked:false})
  </HTMLElement><params>'input'</params>
  </pipe><params>todoContainer</params>
  </void><js>
  addTodoEl.addEventListener("click",outputValues)
  destroyTodosEl.addEventListener("click",()=>destroyTodos(todoContainer))
  -> outputValues
  }</js></block>
  
  const todoMethods = <object>
  <js>
  const createTodo = (container) => createElement('p',container)
  const destroyTodos = (container) => container.innerHTML = ''
  const addTodoProps = (elFn,text) => elFn({textContent:text})
  -> {createTodo,destroyTodos,addTodoProps}
  </js>
  </object>
  
   const specialTodoMethods = <object><js>
  const deleteSelectedTodo = (id) =>{
    const el = document.getElementsByClassName('todo')[id]
    el.parentNode.removeChild(el)
  }
  -> {deleteSelectedTodo}
  </js>
  </object>
  
  
    const todoSelector = <args>methods,elements</args>
  				    <block>
                          <js>
    const {deleteSelectedTodo} = methods()
    const [deleteSelected,numberField,todoContainer] = 
    selectElementsById(...elements)
                          </js>
	<js>
  if(addTodoEl){
  deleteSelected.addEventListener("click",()=>deleteSelectedTodo(numberField.value))
  }</js></block>
   
  const App = <comp>
  			 <HTMLElement> -> addRawHTML(
                 document.body,
                  `<hr>
                  <p>Dynamicly added HTML</p>
                  <button class="UI" id="addTodoEl" >add</button>
  			   <input class="UI" id="inputField" value="Default todo">
 			   <button class="UI" id="destroyTodosEl" >clear</button>
  			   <div id="todoContainer"></div>`
                  )</HTMLElement><params>'div'</params><./>
  			<func> -> todoComponent(
                 todoMethods,
                 ["addTodoEl","inputField","destroyTodosEl","todoContainer"]) 
  			</func><./>
   			<HTMLElement> -> addRawHTML(
                 document.body,
                  `<hr>
                  <button class="UI" id="deleteSelected" >delete</button>
  			   <input class="UI" id="numberField" 
                   style="width:50px" type="number" value="0">
  			   <div id="todoContainer"></div>`
                  )</HTMLElement><params>'div'</params><./>
  			<func> -> todoSelector(
                 specialTodoMethods,
                 ["deleteSelected","numberField","todoContainer"]) 
  			</func><./>
  			</comp>
 document.addEventListener("DOMready",App);
</jsml>
```




```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  </head>
<body>
<h1 id="timerElement">0</h1>
<jsml/>

</body>
</html>
const timer = <object>
<js>
  let startTime
  let endTime
  let direction
  let timeGap
  let timeInSeconds
  let tickSettings = {startTime:0,endTime:0,speed:1}
</js>
  const addTickEvent =<args>settings,callback</args>
    <block>
    <js>
     startTime = settings.startTime || tickSettings.startTime
     endTime = settings.endTime || tickSettings.endTime
     timeGap = settings.speed || tickSettings.speed
     tickSettings = settings
     timeInSeconds = 0 + startTime || 0
     direction  = startTime <= endTime ? 1 : -1
    if(typeof callback === 'function') 
    </js>tickEvent = <void> -> callback(timeInSeconds)</void>
  </block>

  let timeStamps = [];
  let cacheTimeStamps = [];
  let pause = false;
  let timeout;
  let tickEvent;
  let eventMap = new Map();

  const countTime = <void>
    
      timeout = setTimeout(<void>
	 <js>
      tickEvent();
      if(!pause &&  
         (direction === 1 && timeInSeconds<=endTime) 
         || (direction === -1 && timeInSeconds>=endTime)){
        const currentEvent = eventMap.get(timeInSeconds)
          if(typeof currentEvent === 'function'){
            currentEvent();
          }
        countTime();
        timeInSeconds+=1*direction
      }
      </js>
    </void>,1000*timeGap);
    </void>

  const reachTime =  <void>
    timeout = setTimeout(<void>
     <js>
      if(!pause){
        const current = timeStamps.pop();
        const currentEvent = eventMap.get(current);
          if(typeof currentEvent === 'function') currentEvent()
          reachTime()
      }
     </js>
     </void>,timeStamps[timeStamps.length-1]*1000);
  </void>

  const addTimeEvents = <args>...events</args><block>
    eventMap = <fold>
      if(!Number.isInteger(current[0])){
       throw new Error('Event Time should be a valid integer - but instead got:' + current[0])
      }
      output.set(current[0], current[1])
    </fold map>(events)

    timeStamps = <sort> -> b-a</sort>([...eventMap.keys()])
    cacheTimeStamps = [...timeStamps]
  
</block>
  const start = <void>
    pause = false
   if(!timeout){
    if(!tickEvent)reachTime()
    else countTime()
   }
  </void>
  const stop = <void>
    pause = true
    clearTimeout(timeout)
  </void>
  const reset = <void> 
    pause = true;
    startTime = tickSettings.startTime || 0
    timeInSeconds = 0 + startTime
    timeStamps = [...cacheTimeStamps]
    clearTimeout(timeout)
  </void>
  -> {start,stop,reset,addTimeEvents,addTickEvent}
</object>

<pipe>
<object><js> 
const element = document.getElementById('timerElement')
const timer = input
timer.addTickEvent({startTime:0,endTime:20,speed:1},(currentTime)=>element.innerHTML = currentTime); //optional
-> {timer,element}</js>
</object><./>
<object><js> 
const {timer,element} = input</js>
timer.addTimeEvents(
  [5,<void> 
   element.style.color = 'red' 
  </void>],
  [10,<void> 
   element.style.color = 'black' 
  </void>],
  [20,<void> 
   timer.reset()
   timer.start() 
  </void>])
-> timer 
</object><./>
<object><js> 
input.start()
-> input </js>
</object>
</pipe><params>timer()</params>


```





```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  </head>
<body>
<h1 id="timerElement">0</h1>
<jsml/>

</body>
</html>
<iife>
const timer = <object>
<js>
  let startTime
  let endTime
  let direction
  let speed
  let timeInSeconds
  let tickSettings = {startTime:0,endTime:0,speed:1}
</js>
  const addTickEvent =<args>startTimeSetting,endTimeSetting,speedSetting,callback</args>
    <block>
    <js>
     startTime = startTimeSetting || tickSettings.startTime
     endTime = endTimeSetting || tickSettings.endTime
     speed = speedSetting || tickSettings.speed
      
      
     tickSettings = {startTime:startTimeSetting,endTime:endTimeSetting,speed:speedSetting}
     timeInSeconds = 0 + startTime || 0
     direction  = startTime <= endTime ? 1 : -1
    if(typeof callback === 'function') 
    </js>tickEvent = <void> -> callback(timeInSeconds)</void>
  </block>

  let timeStamps = [];
  let cacheTimeStamps = [];
  let pause = false;
  let timeout;
  let tickEvent;
  let eventMap = new Map();

  const countTime = <void>
    
      timeout = setTimeout(<void>
	 <js>
      tickEvent();
      if(!pause &&  
         (direction === 1 && timeInSeconds<=endTime) 
         || (direction === -1 && timeInSeconds>=endTime)){
        const currentEvent = eventMap.get(timeInSeconds)
          if(typeof currentEvent === 'function'){
            currentEvent();
          }
        countTime();
        timeInSeconds+=1*direction
      }
      </js>
    </void>,1000*speed);
    </void>

  const reachTime =  <void>
    timeout = setTimeout(<void>
     <js>
      if(!pause){
        const current = timeStamps.pop();
        const currentEvent = eventMap.get(current);
          if(typeof currentEvent === 'function') currentEvent()
          reachTime()
      }
     </js>
     </void>,timeStamps[timeStamps.length-1]*1000);
  </void>

  const addTimeEvents = <args>...events</args><block>
    eventMap = <fold>
      if(!Number.isInteger(current[0])){
       throw new Error('Event Time should be a valid integer - but instead got:' + current[0])
      }
      output.set(current[0], current[1])
    </fold map>(events)

    timeStamps = <sort> -> b-a</sort>([...eventMap.keys()])
    cacheTimeStamps = [...timeStamps]
  
</block>
  const start = <void>
    pause = false
   if(!timeout){
    if(!tickEvent)reachTime()
    else countTime()
   }
  </void>
  const stop = <void>
    pause = true
    clearTimeout(timeout)
  </void>
  const reset = <void> 
    pause = true;
    startTime = tickSettings.startTime || 0
    timeInSeconds = 0 + startTime
    timeStamps = [...cacheTimeStamps]
    clearTimeout(timeout)
  </void>
  -> {start,stop,reset,addTimeEvents,addTickEvent}
</object>
-> <void>
const {element,context} = input
<pipe>
<func>
    -> <curry>
  input.addTickEvent
  </curry></func>,
  <func>
  -> input(0)
  </func>,
  <func>
  -> input(20)
  </func>,
  <func>
  -> input(1)
  </func>,
<object>
      input((currentTime)=>{element.innerHTML = currentTime})
   -> {timer:context,el:element}
  </object>,
<object>
const {timer,el} = input
timer.addTimeEvents(
  [5,<void> 
   el.style.color = 'red' 
  </void>],
  [10,<void> 
   el.style.color = 'black' 
  </void>],
  [20,<void>
   timer.reset()
   timer.start() 
  </void>])
-> timer
</object>,
<object><js> 
input.start()
-> input </js>
</object>
</pipe><params>
context
</params>
  
</void><params>
  {
  context:timer(),
  element:document.getElementById('timerElement')
  }
</params>
  
</iife>
```

```
-> <number>
     -> <fold>
  	    output += current
  	   </fold number><params>
  	  <if><js>
          [()=>input>5,()=>1],
          [()=>input%2===0,()=>1],
          [()=>input<10, ()=>1]</js>
       </if>
  	  </params>
 </number><params> 7 </params>
```


```
-> (<args>nums, target</args>
	 <block>
        let out = 0;
        <find> 
          if(current === target || current > target){
          out=index
          return true;
          }else if(current > target){
           out=index
          return true
          }else{
          out = nums.length
          return false
          }
      
        </find><params>nums</params>
-> out
</block>)<params>
[1,3,5,6],5</params>
```

```
const inv = <void>
-> <curry><args>a,b,c,d</args>
<block>
-> <pipe>
<number>
-> input*12*b
</number><./>
<number>
-> input-3*c  
</number><./>
<string>
-> input.toString()  + d  
</string><./>
<array>
-> input.split('') 
</array><./>
<scan> 
-> Number(current)  
</scan>
</pipe><params>a</params>
</block>
</curry>
</void>
-> inv()(15)(2)(34)('123322')

```